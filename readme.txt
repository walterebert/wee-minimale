=== wee minimale ===
Contributors: walterebert
Tags: accessibility-ready, full-site-editing, translation-ready
Requires at least: 6.6
Tested up to: 6.6
Requires PHP: 7.2
Version: 0.2.0
License: GPL-2.0-or-later
License URI: https://spdx.org/licenses/GPL-2.0-or-later.html

A bare minimum block theme. No features, no options, no promises.

== Copyright ==
wee minimale, Copyright 2024 Walter Ebert
wee minimale is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
