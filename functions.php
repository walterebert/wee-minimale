<?php
/**
 * Theme functions
 *
 * @package WordPress
 * @subpackage wee minimale
 */

/**
 * Theme setup
 *
 * @since wee minimale 1.0
 */
function wee_minimale_setup() {
	add_theme_support( 'editor-styles' );
	add_editor_style();
}
add_action( 'after_setup_theme', 'wee_minimale_setup', 9 );

/**
 * Enqueue stylesheets.
 *
 * @since wee minimale 1.0
 */
function wee_minimale_styles() {
	wp_enqueue_style(
		'wee-minimale',
		get_template_directory_uri() . '/style.css',
		[],
		wp_get_theme( 'wee-minimale' )->get( 'Version' )
	);
}
add_action( 'wp_enqueue_scripts', 'wee_minimale_styles' );
