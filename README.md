# wee minimale

A bare minimum WordPress block theme:

- No features
- No options
- No promises

## License
[GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html)

## Author
[Walter Ebert](https://wee.press/)
